# Luciole

UCI chess engine written in Lua. Plays traditional chess and Fischer random chess.

## Homepage

The project has been moved to [Codeberg](https://codeberg.org/rchastain/luciole).

## Commands

### UCI commands

  * isready
  * go
  * position
  * uci
  * ucinewgame

### Other commands

  * perft
  * show

## Logo

Thanks to Norbert Raimund Leisner.

![alt text](https://gitlab.com/rchastain/luciole/-/raw/master/logo/luciole.jpg)

## Modules

*Luciole* uses [log][1], [serpent][2] and [strict][3].

[1]: https://github.com/rxi/log.lua
[2]: https://github.com/pkulchenko/serpent
[3]: https://github.com/deepmind/strict
